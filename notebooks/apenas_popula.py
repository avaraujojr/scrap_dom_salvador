#!/usr/bin/env python
# coding: utf-8

# # Bibliotecas

# In[1]:


# Será usada para se armazenar a relação DOM x Data x URL e diversos outros armazenamentos em tabela
import pandas as pd

# Manipulações numéricas
import numpy as np

# Será usada para manipular as datas
import datetime as dt

# Calcular tempo de execução de partes do código
import time

# Será usada para se formar os nomes por extenso dos meses
import locale
locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")


# In[2]:


# Será usada para tentar abrir cada URL
import urllib.request
from urllib.request import urlopen
import requests

# Tratamentos de exceções
from urllib.error import HTTPError # Definição dos erros HTTPError, para evidenciar erros 404, quando as páginas não existem
from urllib.error import URLError # Definição dos erros URLError, quando as páginas existem, mas há outros erros de acesso

# Biblioteca que será usada para procurar as informações dentro dos códigos-fonte das páginas
from bs4 import BeautifulSoup 

# Tratamento de expressões regulares
import re


# In[3]:


# Para as leituras em si dos PDFs
from PyPDF2 import PdfFileReader

# Tratamentos de entrada/saída
import io


# # Funções

# In[4]:


def monta_URL_meses(ano_inicio, ano_final, flag_mes_upper=0):
    dict_dir_meses = {}
    
    global pag_inicial
    
    url_comum = pag_inicial + 'images/stories/pdf/'
    
    for ano in range(ano_inicio, ano_final+1):
        for mes in range(1,13):
            nome_per = str(ano).zfill(2) + '-' + str(mes).zfill(2)
            if(dt.datetime(ano, mes, 1).strftime('%B') == 'março'):
                nome_mes = 'marco'
            else:
                nome_mes = dt.datetime(ano, mes, 1).strftime('%B')
            
            if(flag_mes_upper == 0):
                dict_dir_meses[nome_per] = url_comum + str(ano).zfill(4) + '/' + nome_mes
            
            elif(flag_mes_upper == 1):
                dict_dir_meses[nome_per] = url_comum + str(ano).zfill(4) + '/' + nome_mes.title()
    
    return dict_dir_meses


# In[5]:


def monta_URL_por_data(no_dom, data, prefixo='http', flag_upper_dom=0, flag_mes_upper=0, flag_mes_dif=0):
    global pag_inicial
    
    url_comum = prefixo + pag_inicial[4:] + 'images/stories/pdf/'
    
    if(flag_mes_dif == 0):
        nome_mes = data.strftime('%B')
        ano_data = str(data.year).zfill(4)
    else:
        if(flag_mes_dif < 0):
            data_ref = dt.date(data.year, data.month, 1) - dt.timedelta(1)
        elif(flag_mes_dif > 0):
            data_ref = dt.date(data.year, data.month, 1) + dt.timedelta(31)
        
        nome_mes = data_ref.strftime('%B')
        ano_data = str(data_ref.year).zfill(4)
    
    nome_dom = 'dom'
    
    
    if(nome_mes == 'março'):
        nome_mes = 'marco'
    
    if(flag_mes_upper == 1):
        nome_mes = nome_mes.title()
        
    if(flag_upper_dom == 1):
        nome_dom = nome_dom.upper()
    
    data_str = data.strftime('%d-%m-%Y')
    
    end_URL = url_comum + ano_data + '/' + nome_mes + '/' + nome_dom + '-' + no_dom + '-' + data_str + '.pdf'
    
    end_URL = end_URL.replace('arço', 'arco')
    
    return end_URL


# In[6]:


def verifica_url(download_url):
    a = ''
    try:
        response = urllib.request.urlopen(download_url)    
        #response = urlopen(download_url)    
    
    except HTTPError as e:
        a = 'ERRO HTTPError!'
        
    except URLError as e:
        a = 'ERRO URLError!'
    
    else:
        a = 'SUCESSO'
        
    return a


# In[7]:


# Função que calcula a menor e a maior data possível para um DOM não encontrado
def calcula_menor_maior_data(df, no_dom):
    df_calc = df.copy()

    no_dom_int = int(no_dom)
    
    df_calc = df_calc.rename(columns={'No_DOM':'No_DOM_INT'})
    df_calc['No_DOM_INT'] = df_calc['No_DOM_INT'].astype(int)
    
    data_menor = df_calc.loc[df_calc['No_DOM_INT'] < no_dom_int].sort_values(by='No_DOM_INT', ascending=False).head(1).reset_index(drop=True).iloc[0,1]
    data_maior = df_calc.loc[df_calc['No_DOM_INT'] > no_dom_int].sort_values(by='No_DOM_INT', ascending=True).head(1).reset_index(drop=True).iloc[0,1]
    
    lista_datas = [data_menor, data_maior]
    return lista_datas


# In[8]:


def converte_pd_timestamp(timestamp):
    data_correta_txt = timestamp.strftime('%Y-%m-%d')
    ano = data_correta_txt[:4]
    mes = data_correta_txt[:7][-2:]
    dia = data_correta_txt[-2:]
    data_correta = dt.date(int(ano), int(mes), int(dia))
    return data_correta


# In[9]:


def extrai_n_dom_do_URL(url):
    if(re.search('/dom', url) != None):
        n_dom = url[re.search('/dom', url).start():][5:9]
        return n_dom
    else:
        return 'ERRO'

def extrai_data_URL(url):
    exp_data = '[0-9]{2}\-[0-9]{2}\-[0-9]{4}'
    
    if(re.search(exp_data, url) != None):
        data = url[re.search(exp_data, url).start():][:10]
        data = dt.datetime.strptime(data, '%d-%m-%Y').date()
        return data
    else:
        return dt.date(1900,1,1)


# In[10]:


def info_creators(url_pdf):
    response = requests.get(url_pdf)
    
    creator_status = ''
    
    if(response.status_code != 200):
        print(f'\nHÁ ALGUM ERRO AO TENTAR ACESSAR O DOM {extrai_n_dom_do_URL(url_pdf)}')
        creator_status = 'ERRO'
        return creator_status
    
    with io.BytesIO(response.content) as f:
        pdf_DOM = PdfFileReader(f)
        creator_status = pdf_DOM.getDocumentInfo()['/Creator']
        
    return creator_status


# # Código em si

# In[11]:


# Primeiro carregamento
# df_all_URL = pd.read_csv('../aux_files/lista_DOM_OCR.csv')
# df_all_URL = df_all_URL.iloc[:,[0,2]]
# df_all_URL['No_DOM'] = df_all_URL['No_DOM'].astype(str).str.zfill(4)
# df_all_URL['Creator'] = ''

# Demais carregamentos
df_all_URL = pd.read_csv('../aux_files/lista_DOM_PDF_creators.csv')
df_all_URL['No_DOM'] = df_all_URL['No_DOM'].astype(str).str.zfill(4)
df_all_URL['Creator'] = df_all_URL['Creator'].fillna('')
df_all_URL


# In[12]:


#for i in range(len(df_all_URL)):
for i in range(len(df_all_URL)-1,-1,-1):
    if(df_all_URL.iloc[i,2] == ''):
        url_DOM = df_all_URL.iloc[i,1]
        try:
            df_all_URL.iloc[i,2] = info_creators(url_DOM)
            df_all_URL.to_csv('../aux_files/lista_DOM_PDF_creators.csv', index=False)
        except IndexError as e:
            print(f'EXCEÇÃO {e} TRATADA COM SUCESSO - DOM-{df_all_URL.iloc[i,0]}')
            df_all_URL.iloc[i,2] = 'ERRO NA LEITURA'
            pass
        continue


# In[ ]:


df_all_URL['Creator'].value_counts()


# In[ ]:




