# Importação das bibliotecas
import requests
import re

import pandas as pd
import numpy as np

import datetime as dt
import seaborn as sns

import bs4

import urllib.request
from urllib.error import HTTPError
from urllib.error import URLError

from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

from time import sleep

sns.set(rc={"figure.figsize": (14, 6)}, font_scale=1.25)

import locale
locale.setlocale(locale.LC_ALL, "pt_BR.UTF-8")


# Definindo uma string com alguns user-agents conhecidos pelos servidores web, para evitar possível
# bloqueio durante o acesso.
user_agent = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) "
              "AppleWebKit/537.36 (KHTML, like Gecko) "
              "Chrome/39.0.2171.95 "
              "Safari/537.36")

# Definindo os cabeçalhos que enviaremos junto com a requisição GET
headers = {
    "User-Agent": user_agent
}


def preenche_dom_lista_inicial(df_vazio):
    n = 8158
    data_inicial = datetime.date(2021,11,23)
    
    molde_geral_p1 = 'http://www.dom.salvador.ba.gov.br/images/stories/pdf/'
    #molde_geral_p2 = '2021/novembro/dom-8157-22-11-2021.pdf'
    
    for i in range(n):
        data = data_inicial
    return df_populado


df_dom_datas = pd.DataFrame(data={'NUMERO_DOM':[], 'DATA':[], 'URL_PDF':[]})

dict_meses = {1:'janeiro', 2:'fevereiro', 3:'marco', 4:'abril', 5:'maio', 6:'junho', 7:'julho', 8:'agosto', 9:'setembro', 10:'outubro', 11:'novembro', 12:'dezembro'}


molde_geral_p1 = 'http://www.dom.salvador.ba.gov.br/images/stories/pdf/'

cabec = requests.get(molde_geral_p1, headers=headers)

print(cabec.text)


lista_url_meses = list(pd.read_csv('../aux_files/lista_url_meses_http.csv').iloc[:,0])


from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError
from bs4 import BeautifulSoup
from re import search

lista_todos_DOM = []
dicio_todos_DOM = {}


for url_mes in lista_url_meses:

    try:
        html = urlopen(url_mes)
        bs = BeautifulSoup(html, 'html.parser')
    
    except HTTPError as e:
        print(f'Houve erro do tipo HTTPError ao tentar acessar {url_mes}')
        print(e)
        
    except URLError as e:
        print(f'Houve erro do tipo URLError ao tentar acessar {url_mes}')
        print(e)
        
    else:
        #print(f'Houve sucesso ao se tentar acessar o URL {url_mes}')
        pdf_list = bs.findAll("a")
        for name in pdf_list:
            lista_todos_DOM.append(name.get_text())
            dicio_todos_DOM.update({url_mes:name.get_text()})


# MONTAGEM DE UM DATAFRAME DE BASE
df_leitura_DOM = pd.DataFrame(data={'Nome_arquivo':lista_todos_DOM})

df_leitura_DOM = df_leitura_DOM[df_leitura_DOM['Nome_arquivo'].str.contains('\.pdf')]

df_leitura_DOM['Nome_arquivo'] = df_leitura_DOM['Nome_arquivo'].str.replace('\.','-')

df_leitura_DOM = pd.concat([df_leitura_DOM, df_leitura_DOM['Nome_arquivo'].str.split('-', expand=True).iloc[:,[1,2,3,4]]], axis=1)

df_leitura_DOM = df_leitura_DOM.rename(columns={1:'No_DOM', 2:'Dia', 3:'Mes', 4:'Ano'})
df_leitura_DOM['Dia'] = df_leitura_DOM['Dia'].astype(int)
df_leitura_DOM['Mes'] = df_leitura_DOM['Mes'].astype(int)
df_leitura_DOM['Ano'] = df_leitura_DOM['Ano'].astype(int)

df_leitura_DOM.insert(4,'Mes_nome', value='')

df_leitura_DOM['Mes_nome'] = df_leitura_DOM.apply(lambda x: dict_meses[x['Mes']], axis=1)


df_leitura_DOM['Nome_arquivo'] = df_leitura_DOM['Nome_arquivo'].str.replace('-pdf','.pdf')

df_leitura_DOM['Data'] = df_leitura_DOM.apply(lambda x: dt.date(x['Ano'], x['Mes'], x['Dia']), axis=1)

#df_leitura_DOM['URL'] = df_leitura_DOM['Pasta_mes'] + '/' + df_leitura_DOM['Nome_arquivo']

#df_leitura_DOM = df_leitura_DOM.iloc[:,[2,3,4,5,6,1,7]]

df_leitura_DOM['Caps'] = ''

df_leitura_DOM['Caps'] = df_leitura_DOM.apply(lambda x: 'Sim' if x['Nome_arquivo'].startswith('DOM') else 'Não', axis=1)


df_leitura_DOM = df_leitura_DOM.drop_duplicates()


df_leitura_DOM['URL'] = ''

df_leitura_DOM['URL'] = df_leitura_DOM.apply(lambda x: molde_geral_p1 + str(x['Ano']) + '/' + x['Mes_nome'] + '/' + x['Nome_arquivo'],axis=1)


# FUNÇÃO QUE MONTA UMA DATA
def monta_URL_data(no_dom, data, upper=0, prefixo='https'):
    parte_comum = prefixo + molde_geral_p1[4:]
    
    data_mostrar = data.strftime('%d-%m-%Y')
    
    nome_mes = dict_meses[int(data.month)]
    
    if(type(no_dom) != type('aaa')):
        no_dom = str(no_dom).zfill(4)
    
    dom = 'dom'
    
    if(upper!=0):
        dom = 'DOM'
    
    url = parte_comum + str(data.year) + '/' + nome_mes + '/' + dom + '-' + no_dom + '-' + data_mostrar + '.pdf'
    
    return url


# Alimentação dos campos já preenchidos - ARQUIVO TEMPORÁRIO
df_ultimos = pd.read_csv('../aux_files/URLs_nao_encontrados_temp.csv')

nomes_arquivos = list(df_ultimos['URLs'].str.split('/', expand=True).iloc[:,8])
num_dom = list(df_ultimos['URLs'].str.rstrip('\.pdf').str.split('-', expand=True).iloc[:,1])
dias = list(df_ultimos['URLs'].str.rstrip('\.pdf').str.split('-', expand=True).iloc[:,2])
meses = list(df_ultimos['URLs'].str.rstrip('\.pdf').str.split('-', expand=True).iloc[:,3])
anos = list(df_ultimos['URLs'].str.rstrip('\.pdf').str.split('-', expand=True).iloc[:,4])
caps = list(df_ultimos['URLs'].str.replace('\.pdf','-Não').str.split('-', expand=True).iloc[:,5])
nomes_urls = list(df_ultimos['URLs'])


df_added = pd.DataFrame({'Nome_arquivo':nomes_arquivos, 'No_DOM':num_dom, 'Dia':dias, 'Mes':meses, 'Ano':anos,'Caps':caps,'URL':nomes_urls})

df_added['Dia'] = df_added['Dia'].astype(int)
df_added['Mes'] = df_added['Mes'].astype(int)
df_added['Ano'] = df_added['Ano'].astype(int)

df_added.insert(5,'Data','')

df_added['Data'] = df_added.apply(lambda x: dt.date(x['Ano'], x['Mes'], x['Dia']), axis=1)

print(f'Antes do merge, df_leitura_DOM agora tem o tamanho {len(df_leitura_DOM)}')
df_leitura_DOM = df_leitura_DOM.append(df_added, ignore_index=True)
df_leitura_DOM = df_leitura_DOM.drop_duplicates()
print(f'df_leitura_DOM agora tem o tamanho {len(df_leitura_DOM)}')

# Dicionário contendo uma associação entre os números de DOM e os endereços URL encontrados, 
# para se saber quais são os números de DOM que já foram encontrados e quais estão pendentes de serem encontrados

dict_DOM_URL = {}

for i in range(len(df_leitura_DOM)):
    dict_DOM_URL[df_leitura_DOM.iloc[i,1]] = df_leitura_DOM.iloc[i,8]
    
    
# Criação de uma lista com os números de DOM faltantes, que precisarão ser encontrados por outros métodos
lista_faltantes = []

for i in range(1,8163):
    if (str(i).zfill(4)) not in dict_DOM_URL:
        lista_faltantes.append(str(i).zfill(4))


# FUNÇÃO QUE ACESSA PDFs ONLINE
def download_file(download_url):
    a = ''
    try:
        response = urllib.request.urlopen(download_url)    
    
    except HTTPError as e:
        a = 'ERRO HTTPError!'
        
    except URLError as e:
        a = 'ERRO URLError!'
    
    else:
        a = 'SUCESSO'
        
    return a


# Monta uma lista com todos os possíveis endereços para os DOM não encontrados:

lista_URL_nao_encontrados = []
dict_URL_nao_encontrados = {}
dict_URL_nao_encontrados_datas = {}

# for i in lista_faltantes:
#     for ano in range(1986,2022):
#         for mes in dict_meses:
#             for dia in range(1,32):
#                 lista_URL_nao_encontrados.append(monta_URL_data_http(i,ano,mes,dia,upper=0))
#                 lista_URL_nao_encontrados.append(monta_URL_data_http(i,ano,mes,dia,upper=1))


datas_falt = []
data_in = dt.date(1987,9,3)
data_hoje = dt.date.today()

lista_faltantes.sort(reverse=True)

for i in lista_faltantes:
#    for j in range(int(i), (data_hoje - data_in).days + 1):
    if((int(i) >= 5766) & (int(i) < 5904)):
        int_min = (dt.date(2012,12,29) - dt.date(1987,9,3)).days
        int_max = (dt.date(2013,8,1) - data_in).days
    
    elif((int(i) >= 5904) & (int(i) < 6006)):
        int_min = (dt.date(2013,8,1) - dt.date(1987,9,3)).days
        int_max = (dt.date(2013,12,28) - data_in).days
        
    elif((int(i) >= 6006) & (int(i) < 6494)):
        int_min = (dt.date(2013,12,28) - dt.date(1987,9,3)).days
        int_max = (dt.date(2016,1,5) - data_in).days
    
    elif((int(i) >= 6494) & (int(i) < 7010)):
        int_min = (dt.date(2016,1,5) - dt.date(1987,9,3)).days
        int_max = (dt.date(2018,1,5) - data_in).days
        
    elif((int(i) >= 7010) & (int(i) < 8075)):
        int_min = (dt.date(2018,1,1) - dt.date(1987,9,3)).days
        int_max = (dt.date(2021,7,27) - data_in).days
        #int_max = (data_hoje - data_in).days
        #int_max = int_min + 365
    
    elif((int(i) >= 8075)):
        int_min = (dt.date(2021,7,27) - dt.date(1987,9,3)).days
        int_max = (data_hoje - data_in).days
    
    else:
        int_min = int(i)
        int_max = int(i) + 365
    
    if(int(i) < 8162):
        #int_max = (dict_URL_nao_encontrados_datas[str(int(i) + 1)] - dt.date(1987,9,3)).days
            int_max = (dict_URL_nao_encontrados_datas[min(dict_URL_nao_encontrados_datas.keys())] - dt.date(1987,9,3)).days
    
    for j in range(int_max+2, int_min-2,-1):
    #for j in range(int_max+2, int_max-20,-1):
        data_teste = data_in + dt.timedelta(j)
        ct_certo = 0
        
        if((data_teste - dt.date.today()).days < 0):
            if(download_file(monta_URL_data(i, data_teste, upper=0, prefixo='http')) == 'SUCESSO'):
                lista_URL_nao_encontrados.append(monta_URL_data(i, data_teste, upper=0, prefixo='http'))
                dict_URL_nao_encontrados[i] = monta_URL_data(i, data_teste, upper=0, prefixo='http')
                dict_URL_nao_encontrados_datas[i] = data_teste
                ct_certo = ct_certo + 1
                #break
            
            if(download_file(monta_URL_data(i, data_teste, upper=0, prefixo='https')) == 'SUCESSO'):
                lista_URL_nao_encontrados.append(monta_URL_data(i, data_teste, upper=0, prefixo='https'))
                dict_URL_nao_encontrados[i] = monta_URL_data(i, data_teste, upper=0, prefixo='https')
                dict_URL_nao_encontrados_datas[i] = data_teste
                ct_certo = ct_certo + 1
                #break
            
            if(download_file(monta_URL_data(i, data_teste, upper=1, prefixo='http')) == 'SUCESSO'):
                lista_URL_nao_encontrados.append(monta_URL_data(i, data_teste, upper=1, prefixo='http'))
                dict_URL_nao_encontrados[i] = monta_URL_data(i, data_teste, upper=1, prefixo='http')
                dict_URL_nao_encontrados_datas[i] = data_teste
                ct_certo = ct_certo + 1
                #break
            
            if(download_file(monta_URL_data(i, data_teste, upper=1, prefixo='https')) == 'SUCESSO'):
                lista_URL_nao_encontrados.append(monta_URL_data(i, data_teste, upper=1, prefixo='https'))
                dict_URL_nao_encontrados[i] = monta_URL_data(i, data_teste, upper=1, prefixo='https')
                dict_URL_nao_encontrados_datas[i] = data_teste
                ct_certo = ct_certo + 1
                #break
        
        print(f'DOM {i} - Data: {data_teste} - Quantidade de endereços já adicionados: {len(lista_URL_nao_encontrados)}\n', end='\r', flush=True)
        
        if(ct_certo > 0):
            print(f'CHEGOU NA DATA CERTA - DOM {i} - Data: {data_teste}\n')
            break

print(len(lista_URL_nao_encontrados))